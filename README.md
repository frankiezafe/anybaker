# AnyBaker

tiny [openframeworks](http://openframeworks.cc) app that bakes informations to texture

it has been developped to circumvent limitations in the baking process of [blender](http://blender.org), and increase a lot the renedring process

the app uses a text file as input and save a png on the disk

## what this is doing

### 3d vector (normal, tangent, etc)

let say you have informations of a 3d model to bake into a texture

in this case, we are going to bake the **normal of the vertices**, wich is not a default feature of blender ( it bakes normal of the faces by default )

![](https://polymorph.cool/wp-content/uploads/2018/11/heart_cleanup_screenshot.png)

load and run the python/vertex_normal_per_face_exporter.py in your project, it will generate a **vertex_normal_per_face.txt** file next to the blend file and save the uv map into it

once AnyBaker is compiled, run it in a terminal:

    ./baker -w 1024 -i [absolute path to]/vertex_normal_per_face.txt -o [absolute path to]/baker_001.png

you will get this kind of image:

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_001-300x300.png)

with the same data, if you invert Y axis, you will get this (adding **-iv** in the arguments):

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_002_yswap-300x300.png)

and if you start to mess up with the map, you may end up with:

**+x+z+y**

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_003_colorswap_yswap-300x300.png)

**+x-z+y**

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_004_colorswap_yswap-300x300.png)

if you are using gutters, shapes will expand at borders, just add **-g [int]** to specify gutter width (in pixels)

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_005_gutters-300x300.png) ![](https://polymorph.cool/wp-content/uploads/2018/11/baker_4096_cropped_512-300x300.png)

complete command

    ./baker -i [absolute path to]/vertex_normal_per_face.txt -o [absolute path to]/baker_1024.png -w 1024 -m +x+y+z -iv -g 5 -d

### 1d value (weight, id, etc)

load and run the python/vertex_weight_exporter.py in your project, it will generate a **vertex_weight.txt** file next to the blend file and save the weight into it

![](https://polymorph.cool/wp-content/uploads/2018/11/sisyphus_2018.07.30.21.31_screenshot-300x165.png)

once AnyBaker is compiled, run it in a terminal:

    ./baker -i [absolute path to]/vertex_weight.txt -o [absolute path to]/baker_1024.png -w 1024 -t uvw -iv -g 10 -c0 0ff -c1 f00 -d

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_1024-300x300.png)

### rgb value

load and run the python/vertex_color_exporter.py in your project, it will generate a **vertex_color.txt** file next to the blend file and save the rgb into it

once AnyBaker is compiled, run it in a terminal:

    ./baker -i [absolute path to]/vertex_color.txt -o [absolute path to]/baker_1024.png -w 1024 -t uvrgb -iv -g 10 -d

![](https://polymorph.cool/wp-content/uploads/2018/11/baker_1024-1-300x300.png)

## usage

call the exec with arguments:

* **-i** [str]: input data, a text file containing info to bake
* **-s** [str]: character to use to split number, comma by default
* **-t** [str]: data type, explain how to extract data from file, *uvxyz* by default
* **-w** [int]: size of the texture to export
* **-g** [int]: gutter size, must be positive, 0 by default
* **-gd** [int]: gutter depth, used to avoid overlapping of gutters, -1 by default
* **-c0** [str]: minimum color when using only 1 dimension values, hexadecimal, *000000FF* by default
* **-c1** [str]: maximum color when using only 1 dimension values, hexadecimal, *FFFFFFFF* by default
* **-m** [str]: map, allowing color channels switches and inversions
* **-o** [str]: path to output png, will be render next to the input data if not defined
* **-iu**: inversion of the x axis in texture space, false by default
* **-iv**: inversion of the y axis in texture space, false by default
* **-ndt**: no depth test, false by default
* **-d**: debug flag, will visualise faces in the export
* **-h**: display help and quit

### data type

AnyBaker awaits an input data formated like this:

* each line is a polygon with minimum 3 and maximum 4 vertices, any other number of vertices will not be rendered (bye bye ngons...)
* for each vertice, values will be extracted accordingly to specified *data type*

each line is a serie of numbers separated by comma representing the succession of values of vertices

these numbers will be read in bunch to recreate vertices, depending on the specified data type

allowed characters in data type are: *u*, *v*, *x*, *y*, *z*, *w*, *r*, *g*, *b*, *a*

* *u* & *v* will be used to position the vertex in texture space
* if *x*, *y* or *z* is present, the color will be generated from a vector 3, meaning that each axis of the vector will influence a color channel, accordingly to the **map**
* if *x*, *y* or *z* is present and *w* is also specified, the value of *w* will be used as the alpha channel of the color
* if only *w*  is present, minimum and maximum colors will be used to generate the color
* if *r*, *g*, *b* or *a* is present, the color is directly loaded from data, allowing you to create it directly from python script

**priority**

* presence of *r*, *g*, *b* or *a* discard all other color processing
* presence of *x*, *y* or *z* discard usage of min and max colors

if nothing is specified for color generation, all shapes will be black...

**char order**

the order of characters is important! if you extracted info from blender in a different order, adapt the order of characters in data type:

* if you have extracted the tangent xyz and after that the uv coordinates, your data type should be **xyzuv** instead of **uvxyz**
* if you have extracted uv coordinates and weight, data type should be **uvw** to use min & max colors

see python/vertex_normal_per_face_exporter.py for an example of a blender script exporting 3d vector

see python/vertex_weight_exporter.py for an example of a blender script exporting 1 dimension value (weight), render with:

### map

a map is a string of 6 characters organised by pair: one sign and an axis

by default, map is **+x+y+z**, meaning that the color channels will follow the xyz of the vector

axis can be swapped by using a maplike this: **+x+z+y** or inverted if you replace the *+* by a *-*

for instance, if you want to swap y & z axis and invert y axis, your map will look like this: **+x+z-y**

in the generated texture, 

* red will represent **x**
* green will represent **z**
* blue will represent **invert of y**

### colors

colors can be specified in 4 formats:

* **FFF**: RGB, with alpha at 1 (each char will be duplicated)
* **FFFF**: RGBA (each char will be duplicated)
* **FFFFFF**: RGB, with alpha at 1
* **FFFFFFFF**: RGBA

lower or uppercase, it does not matter

## compilation

* download and install [openframeworks](https://openframeworks.cc/download/)
* adapt **OF_ROOT** in **config.make** to point to your installation folder

developped on linux mint 18.2 with openframeworks 0.9.8