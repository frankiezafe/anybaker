#pragma once

#include "ofMain.h"

#define accepted_datatype "uvxyzwrgba"

struct config {
    std::string data_path;
    std::string data_separator;
    std::string data_type;
    uint16_t texture_size;
    uint16_t gutter;
    float gutter_depth;
    bool invert_u;
    bool invert_v;
    bool no_depth_test;
    std::string map;
    std::string w_color_0;
    std::string w_color_1;
    std::string output_path;
    bool debug;

    config() :
    data_separator(","),
    data_type("uvxyz"),
    texture_size(1024),
    gutter(0),
    gutter_depth(-1),
    invert_u(false),
    invert_v(false),
    no_depth_test(false),
    map("+x+y+z"),
    w_color_0("000000FF"),
    w_color_1("FFFFFFFF"),
    debug(false) {
    }

    void help() {
        cout << "configuration helper:" << endl <<
                "\t" << "-i: data_path, current: " << data_path << endl <<
                "\t" << "-s: data_separator, current: " << data_separator << endl <<
                "\t" << "-t: data_type, current: " << data_type << endl <<
                "\t" << "-w: texture_size, current: " << texture_size << endl <<
                "\t" << "-g: gutter, current: " << gutter << endl <<
                "\t" << "-gd: gutter_depth, current: " << gutter_depth << endl <<
                "\t" << "-m: map, current: " << map << endl <<
                "\t" << "-c0: w_color_0, current: " << w_color_0 << endl <<
                "\t" << "-c1: w_color_1, current: " << w_color_1 << endl <<
                "\t" << "-o: output_path, current: " << output_path << endl <<
                "\t" << "-ix: invert_u, current: " << invert_u << endl <<
                "\t" << "-iy: invert_v, current: " << invert_v << endl <<
                "\t" << "-ndt: no_depth_test, current: " << no_depth_test << endl <<
                "\t" << "-d: debug, current: " << debug << endl <<
                endl <<
                "valid characters for data type: " << accepted_datatype << endl <<
                "* 'u' & 'v' are used as position of vertices in the texture" << endl <<
                "* if 'x', 'y', or 'z' is present in your datatype, colors will be generated from a vector3" << endl <<
                "  with a mapping from axis to rgb channel controlled by the mask" << endl <<
                "* if there is only a 'w', w_color_0 & w_color_1 will be used to generate colors" << endl <<
                "* if 'x', 'y', or 'z' is present with 'w', the 'w' value will be used as an alpha channel" <<
                endl;
    }

    void print() {
        cout << "config" << endl <<
                "\t" << "data_path: " << data_path << endl <<
                "\t" << "data_separator: " << data_separator << endl <<
                "\t" << "data_type: " << data_type << endl <<
                "\t" << "texture_size: " << texture_size << endl <<
                "\t" << "gutter: " << gutter << endl <<
                "\t" << "gutter_depth: " << gutter_depth << endl <<
                "\t" << "invert_u: " << invert_u << endl <<
                "\t" << "invert_v: " << invert_v << endl <<
                "\t" << "no_depth_test: " << no_depth_test << endl <<
                "\t" << "map: " << map << endl <<
                "\t" << "w_color_0: " << w_color_0 << endl <<
                "\t" << "w_color_1: " << w_color_1 << endl <<
                "\t" << "output_path: " << output_path << endl <<
                "\t" << "debug: " << debug << endl;
    }

    void operator=(const config& src) {
        data_path = src.data_path;
        data_separator = src.data_separator;
        data_type = src.data_type;
        texture_size = src.texture_size;
        gutter = src.gutter;
        gutter_depth = src.gutter_depth;
        invert_u = src.invert_u;
        invert_v = src.invert_v;
        no_depth_test = src.no_depth_test;
        map = src.map;
        w_color_0 = src.w_color_0;
        w_color_1 = src.w_color_1;
        output_path = src.output_path;
        debug = src.debug;
    }
};

struct dot {
    ofVec3f pos;
    ofVec3f normal;
    ofVec4f rgba;
    float value;

    dot(): rgba(0,0,0,1), value(0) {
    }

    dot(const dot& src) {
        pos = src.pos;
        normal = src.normal;
        rgba = src.rgba;
        value = src.value;
    }

    bool operator==(const dot& src) const {
        return (
                pos == src.pos && 
                normal == src.normal && 
                rgba == src.rgba && 
                value == src.value
                );
    }
};

struct polygon {
    vector< dot > dots;
};

struct edge {
    dot* a;
    dot* b;
    vector< polygon* > polys;

    edge() : a(0), b(0) {
    }

    bool contains(dot* _a, dot* _b) const {
        return ( *_a == *a && *_b == *b) || (*_a == *b && *_b == *a);
    }
};

class ofApp : public ofBaseApp {
public:

    ofApp();
    ofApp(const config& c);
    void setup();

private:

    config cfg;
    float gutter_pc;
    
    bool vec3_color;
    bool rgba_color;
    bool w_alpha;
    ofVec4f color0; 
    ofVec4f color1; 

    vector<polygon> polygons;
    vector<edge> edges;
    ofFbo render;

    float parse_hexa( std::string hexa );
    ofVec4f parse_color( std::string& hexa );
    void parse_line(std::string& l);

    void append_edge(polygon*, dot*, dot*);
    void expand_outside_corners(polygon& p);
    void expand_edge(edge& d);
    void render_dot(dot& d, bool color = true);
    void render_edge(edge& d);

};
