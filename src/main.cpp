#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"

//========================================================================

int main(int argc, char **argv) {
    
    std::string current_exec_name = argv[0];
    
    config cfg;
    bool help_request = false;
    
    for ( int i = 1; i < argc; ++i ) {
        std::string k(argv[i]);
        if ( k.compare( "-i" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.data_path = argv[i];
            }
        } else if ( k.compare( "-s" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.data_separator = argv[i];
            }
        } else if ( k.compare( "-t" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.data_type = argv[i];
            }
        } else if ( k.compare( "-w" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.texture_size = atoi( argv[i] );
            }
        } else if ( k.compare( "-g" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.gutter = atoi( argv[i] );
            }
        } else if ( k.compare( "-gd" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.gutter_depth = atof( argv[i] );
            }
        } else if ( k.compare( "-m" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.map = argv[i];
            }
        } else if ( k.compare( "-c0" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.w_color_0 = argv[i];
            }
        } else if ( k.compare( "-c1" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.w_color_1 = argv[i];
            }
        } else if ( k.compare( "-o" ) == 0 ) {
            if ( i < argc - 1 ) {
                ++i;
                cfg.output_path = argv[i];
            }
        } else if ( k.compare( "-iu" ) == 0 ) {
            cfg.invert_u = true;
        } else if ( k.compare( "-iv" ) == 0 ) {
            cfg.invert_v = true;
        } else if ( k.compare( "-ndt" ) == 0 ) {
            cfg.no_depth_test = true;
        } else if ( k.compare( "-d" ) == 0 ) {
            cfg.debug = true;
        } else if ( k.compare( "-h" ) == 0 ) {
            help_request = true;
        }
    }
    if ( help_request ) {
        
        cfg.help();
    
    } else {
        
        if ( cfg.debug ) {
            cfg.print();
        }
        ofSetupOpenGL( 100, 100, OF_WINDOW );
        ofRunApp( new ofApp( cfg ) );
    
    }
    

}
