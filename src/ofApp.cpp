#include "ofApp.h"

ofApp::ofApp() {
}

ofApp::ofApp(const config& c) {
    cfg = c;
}

void ofApp::parse_line(std::string& l) {

    vector<string> data = ofSplitString(l, cfg.data_separator);
    vector<string>::iterator itd = data.begin();
    vector<string>::iterator itde = data.end();

    size_t dtl = cfg.data_type.length();
    size_t counter = 0;
    dot d;
    polygon poly;

    std::string collected;

    for (; itd != itde; ++itd, ++counter) {

        if (counter >= dtl) {
            counter = 0;
        }

        switch (cfg.data_type[counter]) {
            case 'u':
                collected += cfg.data_type[counter];
                d.pos.x = atof((*itd).c_str());
                break;
            case 'v':
                collected += cfg.data_type[counter];
                d.pos.y = atof((*itd).c_str());
                break;
            case 'x':
                collected += cfg.data_type[counter];
                d.normal.x = atof((*itd).c_str());
                break;
            case 'y':
                collected += cfg.data_type[counter];
                d.normal.y = atof((*itd).c_str());
                break;
            case 'z':
                collected += cfg.data_type[counter];
                d.normal.z = atof((*itd).c_str());
                break;
            case 'r':
                collected += cfg.data_type[counter];
                d.rgba.x = atof((*itd).c_str());
                break;
            case 'g':
                collected += cfg.data_type[counter];
                d.rgba.y = atof((*itd).c_str());
                break;
            case 'b':
                collected += cfg.data_type[counter];
                d.rgba.z = atof((*itd).c_str());
                break;
            case 'a':
                collected += cfg.data_type[counter];
                d.rgba.w = atof((*itd).c_str());
                break;
            case 'w':
                collected += cfg.data_type[counter];
                d.value = atof((*itd).c_str());
                break;
            default:
                collected.clear();
                break;
        }

        if (collected.compare(cfg.data_type) == 0) {

            // all keys are collected, let's register the dot
            poly.dots.push_back(d);
            collected.clear();

        }

    }

    if (poly.dots.size() > 2) {
        polygons.push_back(poly);
    }


}

float ofApp::parse_hexa(std::string hexa) {

    int out = 0;
    if (hexa.length() == 1) {
        hexa += hexa;
    }
    std::stringstream ss;
    ss << std::hex << hexa;
    ss >> out;
    return out * 0.003921569;

}

ofVec4f ofApp::parse_color(std::string& hexa) {

    ofVec4f out(0, 0, 0, 1);
    switch (hexa.length()) {
        case 3:
            out.x = parse_hexa(hexa.substr(0, 1));
            out.y = parse_hexa(hexa.substr(1, 1));
            out.z = parse_hexa(hexa.substr(2, 1));
            break;
        case 4:
            out.x = parse_hexa(hexa.substr(0, 1));
            out.y = parse_hexa(hexa.substr(1, 1));
            out.z = parse_hexa(hexa.substr(2, 1));
            out.w = parse_hexa(hexa.substr(3, 1));
            break;
        case 6:
            out.x = parse_hexa(hexa.substr(0, 2));
            out.y = parse_hexa(hexa.substr(2, 2));
            out.z = parse_hexa(hexa.substr(4, 2));
            break;
        case 8:
            out.x = parse_hexa(hexa.substr(0, 2));
            out.y = parse_hexa(hexa.substr(2, 2));
            out.z = parse_hexa(hexa.substr(4, 2));
            out.w = parse_hexa(hexa.substr(6, 2));
            break;
        default:
            cout << "ofApp::parse_color ERROR! "
                    "hexadecimal representation must have 3, 4, 6 or 8 digits!" <<
                    endl;
            break;
    }
    return out;

}

void ofApp::setup() {

    gutter_pc = (cfg.gutter * 1.f / cfg.texture_size);
    vec3_color = false;
    rgba_color = false;
    w_alpha = false;

    bool oktogo = true;

    // validation of datatype
    std::string adt(accepted_datatype);
    size_t adtl = adt.length();
    size_t dtl = cfg.data_type.length();

    if (dtl < 3) {
        oktogo = false;
    } else {
        for (size_t i = 0; i < dtl; ++i) {
            bool found = false;
            for (size_t j = 0; j < adtl; ++j) {
                if (cfg.data_type[i] == adt[j]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                oktogo = false;
                break;
            } else {
                switch (cfg.data_type[i]) {
                    case 'x':
                    case 'y':
                    case 'z':
                        vec3_color = true;
                        break;
                    case 'r':
                    case 'g':
                    case 'b':
                    case 'a':
                        rgba_color = true;
                        break;
                    case 'w':
                        w_alpha = true;
                        break;
                }
            }
        }
    }

    if (!oktogo) {

        cout << "invalid data type '" << cfg.data_type << "'!\n"
                "characters must be in this list: '" <<
                accepted_datatype << "\nadapt and re-run." << endl;
        ofExit(0);

    } else {

        if (rgba_color) {
            
            vec3_color = false;
            w_alpha = false;
            
        } else if (w_alpha && !vec3_color) {
            
            w_alpha = false;
            
        }

        if (!vec3_color && !rgba_color) {
            color0 = parse_color(cfg.w_color_0);
            color1 = parse_color(cfg.w_color_1);
        }

    }

    if (oktogo) {

        ofBuffer buffer = ofBufferFromFile(cfg.data_path);

        if (buffer.size()) {

            for (ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {

                std::string line = (*it);
                parse_line(line);

            }

            if (cfg.debug)
                cout << "loading finished with " << polygons.size() << " polygons" << endl;

        } else {

            oktogo = false;
            ofExit(0);

        }

    }

    if (oktogo && !polygons.empty()) {

        // generation of edges
        vector<polygon>::iterator it;
        vector<polygon>::iterator ite;

        vector<edge>::iterator itee;
        vector<edge>::iterator iteee;

        it = polygons.begin();
        ite = polygons.end();

        for (; it != ite; ++it) {
            polygon& p = *it;
            size_t ptnum = p.dots.size();
            for (size_t i = 0; i < ptnum; ++i) {
                size_t j = (i + 1) % ptnum;
                append_edge(&(*it), &p.dots[i], &p.dots[j]);
            }
        }

        if (cfg.gutter > 0) {

            // have all edges, let's check the lonely ones and 
            // append a polygon on the outside

            // first of all: corners detection
            it = polygons.begin();
            ite = polygons.end();

            for (; it != ite; ++it) {
                expand_outside_corners(*it);
            }

            itee = edges.begin();
            iteee = edges.end();
            for (; itee != iteee; ++itee) {
                edge& e = (*itee);
                if (e.polys.size() == 1) {
                    expand_edge(e);
                }
            }
        }

        if (cfg.debug)
            cout << "edges > " << edges.size() << endl;

        render.allocate(cfg.texture_size, cfg.texture_size, GL_RGBA);
        render.begin();

        if (!cfg.no_depth_test) {
            ofEnableDepthTest();
        }
        ofEnableAlphaBlending();
        ofClear(0, 0, 0, 0);

        // avoiding that gutters overlaps actual faces
        std::reverse(polygons.begin(), polygons.end());

        it = polygons.begin();
        ite = polygons.end();

        for (; it != ite; ++it) {

            glBegin(GL_TRIANGLES);

            polygon& p = *it;
            switch (p.dots.size()) {
                case 3:
                    for (uint8_t i = 0; i < 3; ++i) {
                        render_dot(p.dots[i]);
                    }
                    break;
                case 4:
                    for (uint8_t i = 0; i < 3; ++i) {
                        render_dot(p.dots[i]);
                    }
                    for (uint8_t j = 0; j < 3; ++j) {
                        uint8_t i = (j + 2) % 4;
                        render_dot(p.dots[i]);
                    }
                    break;
            }

            glEnd();

        }

        if (cfg.debug) {

            ofDisableDepthTest();
            ofDisableAlphaBlending();

            glBegin(GL_LINES);
            itee = edges.begin();
            iteee = edges.end();
            for (; itee != iteee; ++itee) {
                edge& e = (*itee);
                if (e.polys.size() == 1) {
                    glLineWidth(2.f);
                    glColor3f(0, 0, 0);
                    render_edge(e);
                } else {
                    glLineWidth(1.f);
                    glColor3f(1, 1, 1);
                    render_edge(e);
                }
            }
            glEnd();
        }

        render.end();

        ofPixels pxls;
        render.readToPixels(pxls);

        if (cfg.output_path.empty()) {
            cfg.output_path = cfg.data_path.substr(0, cfg.data_path.find_last_of("/\\") + 1) + "baker.png";
        }

        ofSaveImage(pxls, cfg.output_path);

    }


    ofExit(0);

}

void ofApp::expand_outside_corners(polygon& p) {

    size_t ptnum = p.dots.size();

    if (ptnum < 3 || ptnum > 4) {
        return;
    }

    for (size_t i = 0; i < ptnum; ++i) {

        size_t j = (i + 1) % ptnum;
        size_t k = (i + 2) % ptnum;

        dot* a = &p.dots[i];
        dot* b = &p.dots[j];
        dot* c = &p.dots[k];
        edge * current_e(0);
        edge * next_e(0);

        vector<edge>::iterator it = edges.begin();
        vector<edge>::iterator ite = edges.end();

        for (; it != ite; ++it) {
            if ((*it).contains(a, b)) {
                current_e = &(*it);
            } else if ((*it).contains(b, c)) {
                next_e = &(*it);
            }
            if (current_e != 0 && next_e != 0) {
                break;
            }
        }

        if (current_e == 0 || next_e == 0) {
            return;
        }

        if (current_e->polys.size() == 1 && next_e->polys.size() == 1) {

            // let's expand the corner for point b if it is an outward corner!

            // edge normal:
            ofVec3f dir = p.dots[j].pos - p.dots[i].pos;
            dir.normalize();
            ofVec3f norm = dir.getCrossed(ofVec3f(0, 0, 1));
            ofVec3f next_dir = p.dots[k].pos - p.dots[j].pos;
            next_dir.normalize();

            if (norm.dot(next_dir) < 0) {

                polygon newp;
                dot d00(p.dots[j]);
                newp.dots.push_back(d00);

                dot d01(p.dots[j]);
                d01.pos += (dir * gutter_pc);
                d01.pos.z = cfg.gutter_depth;
                newp.dots.push_back(d01);

                dot d02(d01);
                d02.pos -= (next_dir * gutter_pc);
                d02.pos.z = cfg.gutter_depth;
                newp.dots.push_back(d02);

                dot d03(p.dots[j]);
                d03.pos -= (next_dir * gutter_pc);
                d03.pos.z = cfg.gutter_depth;
                newp.dots.push_back(d03);

                polygons.push_back(newp);

            }

        }
    }

}

void ofApp::expand_edge(edge& d) {

    // seeking other points
    dot* previous = 0;
    dot* next = 0;
    polygon* p = d.polys[0];
    size_t ptnum = p->dots.size();
    for (size_t i = 0; i < ptnum; ++i) {
        if (d.a == &p->dots[i]) {
            size_t h = (i + (ptnum - 1)) % ptnum;
            size_t j = (i + 1) % ptnum;
            if (d.b == &p->dots[h]) {
                previous = &p->dots[j];
            } else {
                previous = &p->dots[h];
            }
        } else if (d.b == &p->dots[i]) {
            size_t h = (i + (ptnum - 1)) % ptnum;
            size_t j = (i + 1) % ptnum;
            if (d.a == &p->dots[h]) {
                next = &p->dots[j];
            } else {
                next = &p->dots[h];
            }
        }
    }

    if (previous == 0 || next == 0) {
        return;
    }

    ofVec3f prev_dir = d.a->pos - previous->pos;
    ofVec3f next_dir = next->pos - d.b->pos;
    prev_dir.normalize();
    next_dir.normalize();

    polygon newp;

    dot d00(*d.a);
    d00.pos = d.a->pos + (prev_dir * gutter_pc);
    d00.pos.z = cfg.gutter_depth;
    newp.dots.push_back(d00);

    dot d01(*d.a);
    newp.dots.push_back(d01);

    dot d02(*d.b);
    newp.dots.push_back(d02);

    dot d03(*d.b);
    d03.pos = d.b->pos - (next_dir * gutter_pc);
    d03.pos.z = cfg.gutter_depth;
    newp.dots.push_back(d03);

    polygons.push_back(newp);

}

void ofApp::append_edge(polygon* p, dot* a, dot* b) {

    vector<edge>::iterator it = edges.begin();
    vector<edge>::iterator ite = edges.end();
    for (; it != ite; ++it) {
        if ((*it).contains(a, b)) {
            (*it).polys.push_back(p);
            return;
        }
    }
    edge e;
    e.a = a;
    e.b = b;
    e.polys.push_back(p);
    edges.push_back(e);

}

void ofApp::render_dot(dot& d, bool color) {

    if (color) {

        if (vec3_color) {
            ofVec3f tmp = (ofVec3f(1, 1, 1) + d.normal) * 0.5;
            ofVec3f color(0, 0, 0);

            if (cfg.map.length() == 6) {
                for (uint8_t i = 0; i < 3; ++i) {
                    char sign = cfg.map[(i * 2) + 0];
                    char axis = cfg.map[(i * 2) + 1];
                    float v = 0;
                    switch (axis) {
                        case 'x':
                            v = tmp.x;
                            break;
                        case 'y':
                            v = tmp.y;
                            break;
                        case 'z':
                            v = tmp.z;
                            break;
                    }
                    if (sign == '-') {
                        v = 1 - v;
                    }
                    color[i] = v;
                }
            }

            if (!w_alpha) {
                glColor3f(color.x, color.y, color.z);
            } else {
                glColor4f(color.x, color.y, color.z, d.value);
            }

        } else if ( rgba_color ) {
            
            glColor4f(d.rgba.x, d.rgba.y, d.rgba.z, d.rgba.w);
            
        } else {

            ofVec4f c = color0 * (1 - d.value) + color1 * d.value;
            glColor4f(c.x, c.y, c.z, c.w);

        }

    }

    float x;
    float y;
    x = d.pos.x;
    y = d.pos.y;
    if (cfg.invert_u) {
        x = 1 - x;
    }
    if (cfg.invert_v) {
        y = 1 - y;
    }
    glVertex3f(x * cfg.texture_size, y * cfg.texture_size, d.pos.z);

}

void ofApp::render_edge(edge& e) {

    render_dot(*e.a, false);
    render_dot(*e.b, false);

}