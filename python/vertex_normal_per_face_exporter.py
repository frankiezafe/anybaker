import bpy, os

o = bpy.data.objects[ 'heart' ]

polygons = o.data.polygons
uv = o.data.uv_layers['uv'].data

path = os.path.dirname( bpy.data.filepath )
f = open( os.path.join( path, 'vertex_normal_per_face.txt' ), 'w' )

for p in o.data.polygons:
	
	inum = len(p.vertices)
	
	for i in range( 0, inum ):
		uv_coord = uv[p.loop_start + i].uv
		v_normal = o.data.vertices[ p.vertices[i] ].normal
		if i > 0:
			f.write( ',' )
		f.write( str( uv_coord[0] ) + ',' + str( uv_coord[1] ) + ',' + str( v_normal[0] ) + ',' + str( v_normal[1] ) + ',' + str( v_normal[2] ) )
	
	f.write( '\n' )
	f.flush()

f.close()