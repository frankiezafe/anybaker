import bpy, os

o = bpy.data.objects[ 'cube' ]
groupid = 1

polygons = o.data.polygons
uv = o.data.uv_layers['UVMap'].data

path = os.path.dirname( bpy.data.filepath )
f = open( os.path.join( path, 'vertex_weight.txt' ), 'w' )

for p in o.data.polygons:
	
	inum = len(p.vertices)
	
	for i in range( 0, inum ):
		uv_coord = uv[p.loop_start + i].uv
		w = o.data.vertices[ p.vertices[i] ].groups[groupid].weight
		if i > 0:
			f.write( ',' )
		f.write( str( uv_coord[0] ) + ',' + str( uv_coord[1] ) + ',' + str( w ) )
	
	f.write( '\n' )
	f.flush()

f.close()