import bpy, os, mathutils

o = bpy.data.objects[ 'cube' ]
groupid = 1

polygons = o.data.polygons
uv = o.data.uv_layers['UVMap'].data

path = os.path.dirname( bpy.data.filepath )
f = open( os.path.join( path, 'vertex_color.txt' ), 'w' )

for p in o.data.polygons:
	
	inum = len(p.vertices)
	
	for i in range( 0, inum ):
		
		uv_coord = uv[p.loop_start + i].uv
		w = o.data.vertices[ p.vertices[i] ].groups[groupid].weight
		normal = o.data.vertices[ p.vertices[i] ].normal.copy()
		bottom = [1,0,0.3]
		color = [0,0,0]
		for j in range( 0, 3 ):
			c = (1 + normal[j]) * 0.5
			ci = 1.0 - c
			color[j] = c * w + ci * (1 - w)
			if w <= 0.35:
				pc = pow( w / 0.35, 2 )
				color[j] = color[j] * pc + bottom[j] * (1-pc)
		
		if i > 0:
			f.write( ',' )
		f.write( str( uv_coord[0] ) + ',' + str( uv_coord[1] ) + ',' + str( color[0] ) + ',' + str( color[1] ) + ',' + str( color[2] ) )
	
	f.write( '\n' )
	f.flush()

f.close()